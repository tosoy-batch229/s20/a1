let number = Number(prompt("Enter a number: "));
console.log("The number you provided is " + number);

if (number >= 50) {
    numberDecrease(number);
} else {
    console.log("The current value is at 50. Terminating the loop.");
}

function numberDecrease(number) {
    for (number; number >= 50; number--) {
        if (number <= 50) {
            console.log("The current value is at 50. Terminating the loop.");
            break;
        }

        if (number % 10 == 0) {
            console.log("The number is divisibe by 10. Skipping the number.");
            continue;
        }

        if (number % 5 == 0) {
            console.log(number);
        }
    }
}

let randomString = "supercalifragilisticexpialidocious";
let noVowels = "";

for (let i = 0; i < randomString.length; i++) {
    if (randomString[i] == "a" || 
        randomString[i] == "e" || 
        randomString[i] == "i" || 
        randomString[i] == "o" || 
        randomString[i] == "u" 
    ) {
        continue;
    } else {
        noVowels += randomString[i];
    }
}

console.log(randomString);
console.log(noVowels);